var request = $.ajax({
    url: "http://localhost:3000/v1/api/formulario", //Script para onde vai a requisicao
    type: "GET", //metodo GET ou POST
    data: { responseJSON: "value" }, //Parametros opicional
    dataType: "json" //Tipo do retorno html, script, json ...
});
request.done(function(msg) {
    console.log(request);
});

request.fail(function(jqXHR, textStatus) {
    console.log("Deu Pau");
});

$(document).ready(function() {
    insereDados();
});

function insereDados() {
    var corpoTabela = $(".container").find("tbody");
    var id = "1";
    var nome = "fulano";
    var email = "fulano@detal.com";
    var telefone = "1199945555";
    var descricao = "meu problema ta osso";
    var linha = novaLinha(id, nome, email, telefone, descricao);

    corpoTabela.append(linha);
    $(".container").slideDown(500);

}

function novaLinha(id, nome, email, telefone, descricao) {
    var linha = $("<tr>");
    var colunaId = $("<td>").text(id);
    var colunaNome = $("<td>").text(nome);
    var colunaEmail = $("<td>").text(email);
    var colunaTelefone = $("<td>").text(telefone);
    var colunaDescricao = $("<td>").text(descricao);

    linha.append(colunaId);
    linha.append(colunaNome);
    linha.append(colunaEmail);
    linha.append(colunaTelefone);
    linha.append(colunaDescricao);

    return linha;
}