
// Pegando o link de navegação
const menu = document.querySelectorAll('.nav-link');
// Pegando a tag que vai conter o conteudo carregado.
const conteudo = document.querySelector("main[bind='true']");

// Adicionando a função para no click dos botões de menu vão passar os dados corretos.
menu.forEach((link) => {
    link.onclick = registrarConteudo;
});
// Função para pegar o html de cada página e renderizar na Index
function registrarConteudo(e, linkDireto) {

    // verificando se tem link direto se houver ele é priorizado.
    if (linkDireto) {
        PegarHTML(linkDireto);
        return
    }

    // Prevenindo se vai haver troca de de telas quando se clicar no link.
    e.preventDefault();
    // pegando o link
    const link = e.target;
    // Removendo a classe active de todos os links
    menu.forEach((link) => link.classList.remove('active'));

    // Adicionar a classe de atividade para o link clicado.
    link.classList.add('active');
    // Pegando o arquivo HTML que está na pasta e escrevendo no component MAIN na tela principal.
    PegarHTML(link.href);
}

//Função para fazer um ajax no servidor e pegar o HTML de acordo com o caminho especificado. 
async function PegarHTML(caminho) {
    fetch(caminho)
        .then(res => {
            if (res.status !== 200) {
                // Se o status for diferente de 200 gerar um erro!
                throw new Error("Not 200 response")
            }
            return res.text();
        })
        .then(res => conteudo.innerHTML = res)
        .catch(err => conteudo.innerHTML = "<h3 style='color: red'>Não Foi possível carregar o conteúdo!</h3>");
}